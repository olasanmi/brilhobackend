<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// auth
Route::post('/logins', 'UserController@login');
//cliente
Route::post('/add/client', 'ClientController@add');
Route::get('/get/clients', 'ClientController@getAll');
Route::post('/delete/clients', 'ClientController@delete');
Route::post('/get/distribuidor/products', 'ClientController@getDistriProd');
Route::post('/change/brands', 'ClientController@changeBrands');
Route::post('/update/clients', 'ClientController@update');
// products
Route::post('/add/products', 'ProductsController@stores');
Route::get('/get/products', 'ProductsController@index');
Route::get('/get/products/user/{id}', 'ProductsController@getByUser');
Route::post('/delete/product', 'ProductsController@destroy');
Route::post('/update/product', 'ProductsController@update');
Route::post('/delete/products/order', 'ProductsController@deleteFromOders');
Route::post('/toggle/products/bags', 'ProductsController@toggleProductBags');
Route::post('/edi/products-img', 'ProductsController@editPhoto');
// orders
Route::post('/save/orders', 'OrdersController@create');
Route::get('/get/orders/{id}', 'OrdersController@get');
Route::post('/delete/orders', 'OrdersController@destroy');
Route::post('/update/orders', 'OrdersController@update');
Route::post('/update/order/active', 'OrdersController@toggleIsActive');
Route::get('/total/orders/month', 'OrdersController@monthOrders');
//proveedor
Route::post('/add/proveedor', 'ProveedorController@store');
Route::get('/get/proveedor', 'ProveedorController@show');
Route::post('/add/factura', 'ProveedorFacturasController@store');
Route::post('/delete/proveedor', 'ProveedorController@destroy');
Route::post('/toggle/status', 'ProveedorController@update');
// index
Route::get('/get/index/data', 'OrdersController@showDataToday');
Route::get('/get/index/data/sell', 'OrdersController@showDataTodaySell');
Route::get('/data/orders', 'OrdersController@dataOrders');
Route::get('/data/more/selling', 'OrdersController@moreSell');
//category
Route::post('/add/category', 'CategoryController@store');
Route::post('/edit/category', 'CategoryController@update');
Route::get('/get/category', 'CategoryController@index');
Route::post('/delete/category', 'CategoryController@destroy');
//subcategory
Route::post('/add/subcategory', 'SubCategoryController@add');
Route::post('/edit/subcategory', 'SubCategoryController@update');
Route::get('/get/subcategory', 'SubCategoryController@get');
Route::post('/delete/subcategory', 'SubCategoryController@delete');
//themes
Route::get('/get/themes', 'TematicaController@index');
Route::post('/add/themes', 'TematicaController@store');
Route::post('/edit/themes', 'TematicaController@update');
Route::post('/delete/themes', 'TematicaController@destroy');
// pdf generate
Route::get('generate/pdf/{username}/{category}','ClientController@generatePdf');
Route::get('generate/pdf/product','ProductsController@generatePdf');
