<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    //
    protected $table = 'orders_products';
    protected $fillable = ['id', 'price', 'product', 'ref', 'order_id', 'utility', 'quiantity', 'product_id', 'in_bag'];
    // relations
    public function Order() {
        return $this->belongsTo('App\Order');
    }

    public function Product() {
        return $this->belongsTo('App\Products', 'id');
    }
}
