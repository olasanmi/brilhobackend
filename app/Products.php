<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //
    protected $table = 'products';
    protected $fillable = ['id', 'name', 'subcategory_id', 'category_id', 'price', 'price2', 'price3','default_img', 'value', 'ref', 'position', 'tematica', 'observations', 'type_price', 'grs', 'priceGrs'];
    //relation
    public function Category() {
        return $this->belongsTo('App\Category');
    }

    public function Subcategory() {
        return $this->belongsTo('App\SubCategory');
    }

     public function Tematica() {
        return $this->belongsTo('App\Tematica');
    }

    public function OrderProduct() {
        return $this->hasMany('App\OrderProduct', 'product_id');
    }
    
    public function Cities() {
        return $this->belongsToMany('App\City');
    }
    
}
