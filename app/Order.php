<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table = 'orders';
    protected $fillable = ['id', 'subtotal', 'total', 'domicilio', 'channel', 'client_id', 'utility', 'is_active', 'observations'];
    // relations
    public function OrderProducts() {
        return $this->hasMany('App\OrderProduct', 'order_id');
    }
    //user relation
    public function Client() {
        return $this->belongsTo('App\Client');
    }
    //user relation
    public function OneClient() {
        return $this->hasOne('App\Client', 'id');
    }
}
