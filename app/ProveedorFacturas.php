<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProveedorFacturas extends Model
{
    //
    protected $table = 'proveedor_facturas';
    protected $fillable = ['factura', 'monto', 'status', 'proveedor_id'];
    //relation
    public function Proveedor() {
        return $this->belongsTo('App\Proveedor');
    }
}
