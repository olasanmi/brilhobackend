<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //table
    protected $table = 'client';
    protected $fillable = ['name', 'password','user_id', 'email', 'firts_name', 'last_name', 'address', 'phone', 'gender', 'cedula', 'address_2', 'city', 'city_id', 'price_type'];
    //relation
    public function Orders() {
        return $this->hasMany('App\Orders', 'client_id');
    }
    //
    public function Order() {
        return $this->belongsTo('App\Orders', 'client_id');
    }
}
