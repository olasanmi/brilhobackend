<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    //table
    protected $table = 'sub_category';
    // fillable
    protected $fillable = ['name', 'category_id'];

    //relation
    public function Category() {
        return $this->belongsTo('App\Category');
    }

     //relation
     public function Products() {
        return $this->hasMany('App\Products');
    }
    
}
