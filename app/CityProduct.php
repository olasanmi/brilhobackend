<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityProduct extends Model
{
    //
     //table
     protected $table = 'city_products';
     protected $fillable = ['id', 'city_id','products_id'];
}
