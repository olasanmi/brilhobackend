<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    //table
    protected $table = 'proveedors';
    //fillable
    protected $fillable = ['name'];
    //relation
    public function Facturas() {
        return $this->hasMany('App\ProveedorFacturas', 'proveedor_id');
    }
}
