<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //table
    protected $table = 'categories';
    //fillable
    protected $fillable = ['name', 'id'];
    //relation
    public function Product() {
        return $this->hasMany('App\Products', 'category_id');
    }

    // relation
    public function SubCategory () {
        return $this->hasMany('App\SubCategory', 'category_id');
    }
}
