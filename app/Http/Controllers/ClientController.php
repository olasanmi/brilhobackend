<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use TCPDF;
// models
use App\Client;
use App\User;
use App\Order;
use App\Products;
use App\City;
// resources
use App\Http\Resources\Client\ClientResource;
// helpers
use App\Helpers\Helper;

class ClientController extends Controller
{
    //add new client
    public function add(Request $request) {
        //return response()->json($request->all());
        $client = Client::create([
            'name' => $request->username,
            'firts_name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            'address_2' => $request->address2,
            'city' => $request->city,
            'cedula' => $request->cedula,
            'price_type' => $request->type_price
        ]);
        $userClient = User::create([
            'name' => $request->username,
            'email' => $request->email,
            'password' =>  Hash::make($request->password),
            'user_type' => 2,
            'price_type' => $request->type_price
        ]);
        $city = City::where('name', '=', $request->city)
        ->get();
        $client->city_id = $city[0]->id;
        $client->user_id = $userClient->id;
        $client->save();
        return response()->json(['success' => 'se a creado el cliente correctamente.']);
    }

    //get all clients
    public function getAll() {
        $clients = Client::all();
        return response()->json(['success' => $clients]);
    }

    //update
     //get all clients
     public function update(Request $request) {
        //return response()->json($request->all());
        $client = Client::findOrFail($request->id);
        $client->update([
            'name' => $request->name,
            'firts_name' => $request->firts_name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            'address_2' => $request->address_2,
            'city' => $request->city,
            'cedula' => $request->cedula,
            'price_type' => $request->price_type

        ]);
        
        $user = User::where('id', '=', $client->user_id)
        ->get();
        
        foreach ($user as $key => $value) {
            $value->price_type = $request->price_type;
            $value->save();
        }

        $city = City::where('name', '=', $request->city)
        ->get();
        $client->city_id = $city[0]->id;
        $client->save();
        return response()->json(['success' => $client]);
    }

    //delete client
    public function delete(Request $request) {
        $client = Client::findOrFail($request->client_id);
        $user = User::where('name', '=', $client->name)
        ->get();
        $orders = Order::where('client_id', '=', $client->id)
        ->get();
        foreach ($orders as $key => $value) {
            $value->delete();
        }
        $client->delete();
        $user[0]->delete();
        $clients = Client::all();
        return response()->json(['success' => $clients]);
    }

    //client prods
    public function getDistriProd(Request $request) {
        $array = array();
        $user = User::where('name', '=', $request->username)
        ->get();
        $client = Client::where('user_id', '=', $user[0]->id)
        ->get();
        $orders = Order::where('client_id', '=', $client[0]->id)
        ->where('is_active', '=', 1)
        ->get();
        foreach ($orders as $key => $value) {
            foreach ($value->OrderProducts as $key2 => $value2) {
                $product = Products::findOrFail($value2->product_id);
                $value2['img'] = $product->default_img;
                $value2['category'] = $product->Category;
            }
            $value['products'] = $value->OrderProducts;
            array_push($array, $value);
        }
        return response()->json(['success' => ['client' => $client[0], 'orders' => $orders]]);
    }

    // generate pdf
    public function generatePdf($username, $category) {
        $array = array();
        $user = User::where('name', '=', $username)
        ->get();
        $client = Client::where('user_id', '=', $user[0]->id)
        ->get();
        $orders = Order::where('client_id', '=', $client[0]->id)
        ->where('is_active', '=', 1)
        ->get();
        foreach ($orders as $key => $value) {
            foreach ($value->OrderProducts as $key2 => $value2) {
                $product = Products::findOrFail($value2->product_id);
                $value2['img'] = $product->default_img;
                $value2['category'] = $product->Category;
            }
            $value['products'] = $value->OrderProducts;
            array_push($array, $value);
        }
        $item = array();
       // dd($array);
        foreach ($array as $key => $value) {
            foreach ($value['products'] as $key => $value2) {
                if ($value2['category']->name == $category) {
                    array_push($item, $value2);
                }
            }
        }
        //return response()->json($item);
        $data['products'] = $item;
        $data['client'] = $client[0]->name;
        $data['client_img'] = $user[0]->brand_img;
        $pdf = PDF::loadView('pdf', $data);
        return $pdf->download($client[0]->name.'_catalogo.pdf');
    }

    public function changeBrands(Request $request) {
        $user = User::findOrFail($request->client_id);
        if ($user->brand_img) {
            Helper::deleteFile($user->brand_img, 'brands');
        }
        if ($request->has('file')) {
            $path = Helper::uploadImage($request->file, 'brands');
            $user->brand_img = $path;
            $user->save();
        }
        return response()->json(['success' => $user], 200);
    }
}
