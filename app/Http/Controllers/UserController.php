<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    //login
    public function login(Request $request) {
        try {
            if(Auth::attempt(['name' => $request->username, 'password' => $request->password])){
                return response()->json(['success' => Auth::user()], 200);
            }else{
                return response()->json(['error' => 'Credenciales incorrectos']);
            }
        } catch (\Throwable $th) {
            return response()->json(['error' => $th]);
        }
    }
}
