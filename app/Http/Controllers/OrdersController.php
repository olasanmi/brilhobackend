<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//models
use App\Order;
use App\OrderProduct;
use App\Products;
use App\Client;
use App\User;

class OrdersController extends Controller
{
    //create
    public function create(Request $request) {
        //return response()->json($request->all());
        $client = Client::where('user_id', '=', $request->user['id'])
        ->get();
        $order = Order::create([
            'total' => $request->total,
            'subtotal' => $request->subtotal,
            'domicilio' => $request->domicilio,
            'client_id' => $client[0]->id,
            'observations' =>$request->observations
        ]);
        foreach ($request->products as $key => $value) {
            $orderProduct = new OrderProduct();
            $orderProduct->price = $value['price'];
            $orderProduct->product_id =  $value['id'];
            $orderProduct->product = $value['name'];
            $orderProduct->quiantity = $value['quantity'];
            $orderProduct->order_id = $order->id;
            $orderProduct->ref = $value['ref'];
            $orderProduct->utility = ($value['price'] - $value['value']) * $value['quantity'];
            $orderProduct->save();
        }
        $order['products'] = $order->OrderProducts;
        $order['client'] = $order->Client;
        return response()->json(['success' => $order]);
    }
    //get
    public function get($id) {
        $user = User::findOrFail($id);
        $array = array();
        if ($user->user_type == '1') {
            $orders = Order::all();
            foreach ($orders as $key => $value) {
                foreach ($value->OrderProducts as $key2 => $value2) {
                    $product = Products::findOrFail($value2->product_id);
                    $value2['img'] = $product->default_img;
                    $value2['category'] = $product->Category;
                }
                $value['products'] = $value->OrderProducts;
                $value['client'] = $value->Client;
                array_push($array, $value);
            }
            return response()->json(['success' => $array]);
        } else {
            $client = Client::where('user_id', '=', $id)
            ->get();
            $orders = Order::where('client_id', '=', $client[0]->id)
            ->get();
            foreach ($orders as $key => $value) {
                foreach ($value->OrderProducts as $key2 => $value2) {
                    $product = Products::findOrFail($value2->product_id);
                    $value2['img'] = $product->default_img;
                    $value2['category'] = $product->Category;
                }
                $value['products'] = $value->OrderProducts;
                $value['client'] = $value->Client;
                array_push($array, $value);
            }
            return response()->json(['success' => $array]);
        }
    }
    //destroy
    public function destroy(Request $request) {
        $order = Order::findOrFail($request->order_id);
        $order->delete();
    }
    //update
    public function update(Request $request) {
        //return response()->json($request->all());
        $order = Order::findOrFail($request->order_id);
        $order->total = $request->total;
        $order->subtotal = $request->subtotal;
        $order->domicilio = $request->domicilio;
        $order->save();
        foreach ($request->products as $key => $value) {
            $product = Products::findOrFail($value['id']);
            $orderProduct = new OrderProduct();
            $orderProduct->price = $value['price'];
            $orderProduct->ref =  $value['ref'];
            $orderProduct->product = $value['product'];
            $orderProduct->quiantity = $value['quantity'];
            $orderProduct->order_id = $order->id;
            $orderProduct->utility = ($value['price'] - $product->value) * $value['quantity'];
            $orderProduct->product_id =  $value['id'];
            $orderProduct->save();
        }

        foreach ($request->ollProducts as $key => $value) {
            $orderProduct = OrderProduct::findOrFail($value['id']);
            $orderProduct->quiantity = $value['quiantity'];
            $orderProduct->save();
        }

        $order['products'] = $order->OrderProducts;
        $order['client'] = $order->Client;
        return response()->json(['success' => $order]);
    }
    // show data
    public function showDataToday(Request $request) {
        $ordersToday = array();
        $todayUtility = 0;
        $todaySell = 0;
        $now = Date('yy-m-d');
        $orders = Order::all();
        foreach ($orders as $key => $value) {
            if (substr($value->created_at, 0, 10) == $now) {
                foreach ($value->OrderProducts as $key => $ordersInDay) {
                   $todayUtility = $todayUtility + $ordersInDay->utility;
                   $todaySell = $todaySell + ($ordersInDay->price * $ordersInDay->quiantity);
                }
            }
        }
        $ordersToday['utility'] = $todayUtility;
        $ordersToday['sell'] = $todaySell;
        return response()->json(['success' => $ordersToday]);
    }
     // show data
     public function showDataTodaySell(Request $request) {
        $ordersToday = array();
        $monthUtility = 0;
        $monthSell = 0;
        $now = Date('m');
        $orders = Order::all();
        foreach ($orders as $key => $value) {
            if (substr($value->created_at, 5, 2) == $now) {
                foreach ($value->OrderProducts as $key => $ordersInDay) {
                   $monthUtility = $monthUtility + $ordersInDay->utility;
                   $monthSell = $monthSell + ($ordersInDay->price * $ordersInDay->quiantity);
                }
            }
        }
        $ordersToday['utility'] = $monthUtility;
        $ordersToday['sell'] = $monthSell;
        return response()->json(['success' => $ordersToday]);
    }

    //data orders
    public function dataOrders() {
        $dataArray = array();
        $orders = Order::all();
        foreach ($orders as $key => $order) {
            $data['client'] = $order->Client->name;
            $data['address'] = $order->Client->address;
            $data['total'] = $order->total;
            $utility = 0;
            foreach ($order->OrderProducts as $key2 => $product) {
                $utility = $utility + $product->utility;
                $data['utility'] = $utility;
            }
            array_push($dataArray, $data);
        }
        return response()->json(['success' => $dataArray]);
    }

    //public function moreSell
    public function moreSell() {
        $orders = Order::all();
        $products = products::all();
        $allOrderProd = OrderProduct::all();
        $moreSelling = array();
        foreach ($orders as $key => $order) {
            foreach ($order->OrderProducts as $key2 => $product) {
                foreach ($products as $key => $prodt) {
                    if($prodt->id == $product->ref) {
                        array_push($moreSelling, $prodt);
                    }
                }
            }
        }
        $items = array();
        foreach ($moreSelling as $key => $selling) {
            foreach ($items as $key2 => $item) {
                if ($item->id != $sellin->id) {
                    
                }
            }
        }
    }

    //toggle is active
    public function toggleIsActive(Request $request) {
        $order = Order::findOrFail($request->order_id);
        $order->is_active = !$order->id_active;
        $order->save();
        return response()->json(['success' => $order]);
    }

    //monthOrders total
    public function monthOrders() {
        $array = array();
        $month = date('m');
        $orders = Order::all();
        foreach ($orders as $key => $value) {
            if (substr($value->created_at, 5, 2) == $month) {
                array_push($array, $value);
            }
        }
        return response()->json($array);
    }
}
