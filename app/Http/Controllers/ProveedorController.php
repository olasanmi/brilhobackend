<?php

namespace App\Http\Controllers;

use App\Proveedor;
use App\ProveedorFacturas;
use Illuminate\Http\Request;

class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $proveedor = Proveedor::create([
            'name' => $request->name
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $proveedors = Proveedor::all();
        foreach ($proveedors as $key => $value) {
            $value['facturas'] = $value->Facturas;
        }
        return response()->json(['success' => $proveedors]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function edit(Proveedor $proveedor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $factura = ProveedorFacturas::findOrFail($request->factura_id);
        $factura->status = $request->status;
        $factura->save();
        $proveedor = Proveedor::findOrFail($factura->proveedor_id);
        $proveedor['facturas'] = $proveedor->Facturas;
        return response()->json(['success' => $proveedor]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $proveedor = Proveedor::findOrFail($request->proveedor_id);
        $proveedor->delete();
    }
}
