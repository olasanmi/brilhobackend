<?php

namespace App\Http\Controllers;

use App\ProveedorFacturas;
use Illuminate\Http\Request;

class ProveedorFacturasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $factura = ProveedorFacturas::create([
            'factura' => $request->factura,
            'monto' => $request->monto,
            'status' => $request->status,
            'proveedor_id' => $request->proveedor_id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProveedorFacturas  $proveedorFacturas
     * @return \Illuminate\Http\Response
     */
    public function show(ProveedorFacturas $proveedorFacturas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProveedorFacturas  $proveedorFacturas
     * @return \Illuminate\Http\Response
     */
    public function edit(ProveedorFacturas $proveedorFacturas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProveedorFacturas  $proveedorFacturas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProveedorFacturas $proveedorFacturas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProveedorFacturas  $proveedorFacturas
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProveedorFacturas $proveedorFacturas)
    {
        //
    }
}
