<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//models
use App\Products;
use App\Category;
use App\SubCategory;
use App\CityProduct;
use App\OrderProduct;
use App\City;
use App\User;
use App\Client;
use PDF;
use TCPDF;
//helpers
use App\Helpers\Helper;

class ProductsController extends Controller
{

    public function index()
    {
        //
        $products = Products::all();
        foreach ($products as $key => $value) {
            $value['category'] = $value->Category;
            $value['subcategory'] = $value->SubCategory;
            $value['cities'] = $value->Cities;
        }
        return response()->json(['success' => $products]);
    }

    public function getByUser($id) {
        $array = array();
        $user = User::findOrFail($id);
        $client = Client::where('user_id', '=', $id)
        ->get();
        $city = City::findOrFail($client[0]->city_id);
        foreach ($city->Products as $key => $value) {
            $value['category'] = $value->Category;
            $value['subcategory'] = $value->SubCategory;
            $value['cities'] = $value->Cities;
            array_push($array, $value);
        }
        return response()->json(['success' => $array]);
    }

      public function create()
    {
        //
    }

    public function stores(Request $request)
    {
        // return response()->json($request->all());
        $category = Category::where('name', '=', $request->category)
        ->get();
        $subcategory = SubCategory::where('category_id', '=', $category[0]->id)
        ->where('name', '=', $request->subcategory_id)
        ->get();
        $product = Products::create([
            'name' => $request->name,
            'price' => $request->price,
            'value' => 0,
            'category_id' => $category[0]->id,
            'subcategory_id' => $subcategory[0]->id,
            'position' => $request->position,
            'ref' => $request->ref,
            'observations' => $request->observation,
            'type_price' => $request->typePrice
        ]);

        if ($request->has('typePrice') and $request->typePrice == 'Gramos') {
            $product->grs = $request->grs;
            $product->priceGrs = $request->priceGrs;
        }

        if ($request->has('price2')) {
            $product->price2 = $request->price2;
        }
        
        if ($request->has('price3')) {
            $product->price3 = $request->price3;
        }
        
        $product->save();
        
        if ($request->has('file')) {
            $path = Helper::uploadImage($request->file, 'products');
            $product->default_img = $path;
            $product->save();
        }

        foreach ($request->cities as $key => $value) {
           $city = City::where('name', '=', $value)
            ->get();
            
            if ($city[0]) {
                $cityId = $city[0]->id;
            }

            $productCity = new CityProduct();
            $productCity->city_id = $cityId;
            $productCity->products_id = $product->id;
            $productCity->save();
        }
        
        $tematicaProds = '';

        foreach ($request->tematicas as $key => $value) {
            if ( $key === (count($request->tematicas) - 1) ) {
                $tematicaProds = $tematicaProds.'' .$value . '.';
            } else {
                $tematicaProds = $tematicaProds.$value .', ';
            }
        }

        $product->tematica = $tematicaProds;
        $product->save();

        return response()->json(['success' => $product]);
    }

    public function show(Products $products)
    {
        //
    }

    public function edit(Products $products)
    {
        //
    }


    public function update(Request $request, Products $products)
    {
        $product = Products::findOrFail($request->id);
        $product->update([
            'name' => $request->name,
            'price' => $request->price,
            'value' => 0,
            'ref' => $request->ref,
            'position' => $request->position,
            'observations' => $request->observations,
            'type_price' => $request->type_price
        ]);
        
        if ($request->has('type_price') and $request->type_price == "Gramos") {
            $product->grs = $request->grs;
            $product->priceGrs = $request->priceGrs;
        }

        if ($request->has('type_price') and $request->type_price == "Unidad") {
            $product->grs = 0;
            $product->priceGrs = 0;
        }

        if ($request->has('price2')) {
            $product->price2 = $request->price2;
        }
        if ($request->has('price3')) {
            $product->price3 = $request->price3;
        }
        $product->save();
        
        if ($request->has('categoryNew')) {
            $category = Category::where('name', '=', $request->categoryNew)
            ->get();

            $product->update([
                'category_id' => $category[0]->id
            ]);

            if ($request->has('categoryNew')) {
                $subcategory = SubCategory::where('category_id', '=', $category[0]->id)
                ->where('name', '=', $request->subcategoryNew)
                ->get();

                $product->update([
                    'subcategory_id' => $subcategory[0]->id
                ]);
            }
        }

        if ($request->citiesArrayNew) {
            //return response()->json($request->all());
            $oldCities = CityProduct::where('products_id', '=', $product->id)->get();
            foreach ($oldCities as $key => $value) {
                $oldRelation = CityProduct::findOrFail($value['id']);
                $oldRelation->delete();
            }
            foreach ($request->citiesArrayNew as $key => $value) {
                $city = City::where('name', '=', $value)
                 ->get();
                 
                 if ($city[0]) {
                     $cityId = $city[0]->id;
                 }
                 $productCity = new CityProduct();
                 $productCity->city_id = $cityId;
                 $productCity->products_id = $product->id;
                 $productCity->save();
            }
        }

        $tematicaProds = '';

       if ($request->tematicas) {
            foreach ($request->tematica as $key => $value) {
            if ( $key === (count($request->tematicas) - 1) ) {
                $tematicaProds = $tematicaProds.'' .$value . '.';
            } else {
                $tematicaProds = $tematicaProds.$value .', ';
            }
            $product->tematica = $tematicaProds;
                $product->save();
            }
        }

        

        $product['category'] = $product->Category;
        $product['subcategory'] = $product->SubCategory;
        $product['cities'] = $product->Cities;

        return response()->json(['success' => $product]);
    }

    public function destroy(Request $request)
    {
        $product = Products::findOrFail($request->product_id);
        if ($product->default_img) {
            Helper::deleteFile($product->default_img, 'products');
        }
        $product->delete();

    }

    public function deleteFromOders(Request $request) {
        $product = OrderProduct::findOrFail($request->id);
        $product->delete();
        return response()->json(['success' => 'Se a borrado el producto']);
    }

    public function toggleProductBags(Request $request) {
        $product = OrderProduct::findOrFail($request->id);
        $product->in_bag = $request->in_bag;
        $product->save();
        return response()->json(['success' => 'Se modifico el estado en bolsa']);
    }

    public function generatePdf() {
        $products = Products::all();
        $data['products'] = $products;
        $pdf = PDF::loadView('allProductsPdf', $data);
        return $pdf->download('productos_catalogo.pdf');
    }

    public function editPhoto(Request $request)
    {
        $product = Products::findOrFail($request->id);
        if ($product->default_img) {
            Helper::deleteFile($product->default_img, 'products');
        }

        if ($request->has('file')) {
            $path = Helper::uploadImage($request->file, 'products');
            $product->default_img = $path;
            $product->save();
        }

        return response()->json(['success' => $product]);
    }
}
