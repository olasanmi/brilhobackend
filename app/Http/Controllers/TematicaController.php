<?php

namespace App\Http\Controllers;

use App\Tematica;
use Illuminate\Http\Request;

class TematicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tematica = Tematica::all();
        return response()->json(['success' => $tematica]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $tematica = new Tematica();
        $tematica->name = $request->name;
        $tematica->save();
        return response()->json(['success' => $tematica]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tematica  $tematica
     * @return \Illuminate\Http\Response
     */
    public function show(Tematica $tematica)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tematica  $tematica
     * @return \Illuminate\Http\Response
     */
    public function edit(Tematica $tematica)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tematica  $tematica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $category = Tematica::findOrFail($request->category_id);
        $category->name = $request->category;
        $category->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tematica  $tematica
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $tematica = Tematica::findOrFail($request->theme_id);
        $tematica->delete();
        $tematicas = Tematica::all();
        return response()->json(['success' => $tematica]);
    }
}
