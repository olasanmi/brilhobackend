<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//models
use App\SubCategory;
use App\Category;
use App\Products;

class SubCategoryController extends Controller
{
    //functions
    public function add(Request $request) {
        $category = Category::where('name', '=', $request->category)
        ->get();
        $subcategory = new SubCategory();
        $subcategory->name = $request->name;
        $subcategory->category_id = $category[0]->id;
        $subcategory->save();
        return response()->json(['success' => 'se guardo la subcategoria']);
    }

    //get
    public function get() {
        $array = array();
        $subcategorys = SubCategory::all();
        foreach ($subcategorys as $key => $value) {
            $value['categroy_data'] = $value->Category;
            array_push($array, $value);
        }
        return response()->json(['success' => $array]);
    }

    //delete subcategory
    public function delete(Request $request) {
        $subcategory = SubCategory::findOrFail($request->subcategory_id);
        $subcategory->delete();
        $array = array();
        $subcategorys = SubCategory::all();
        Products::where('subcategory_id', '=', $subcategory->id)->delete();
        foreach ($subcategorys as $key => $value) {
            $value['categroy_data'] = $value->Category;
            array_push($array, $value);
        }
        return response()->json(['success' => $array]);
    }

    // edit
    public function update(Request $request)
    {
        //
        $subcategory = SubCategory::findOrFail($request->subcategory_id);
        $subcategory->name = $request->subcategory;
        $subcategory->save();
    }
}
