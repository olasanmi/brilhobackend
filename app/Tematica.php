<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tematica extends Model
{
    //fillable
    protected $fillable = ['name', 'id'];

    public function Products() {
        return $this->hasMany('App\Product');
    }
}
