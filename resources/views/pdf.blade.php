<!DOCTYPE html>
<html>
<head>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
	<style>
		/** 
			Set the margins of the page to 0, so the footer and the header
			can be of the full height and width !
		 **/
		@page {
			margin: 0cm 0cm;
		}

		/** Define now the real margins of every page in the PDF **/
		body {
			margin-top: 3cm;
			margin-left: 2cm;
			margin-right: 2cm;
			margin-bottom: 2cm;
		}

		/** Define the header rules **/
		header {
			position: fixed;
			top: 0.5cm;
			left: 1cm;
			right: 1cm;
			height: 2cm;
		}

		/** Define the footer rules **/
		footer {
			position: fixed; 
			bottom: 0cm; 
			left: 0cm; 
			right: 0cm;
			height: 2cm;
			text-align: center;
			color: dimgrey;
			font-size: 12px
		}
	</style>
</head>
<body>
	<header>
		<div style="width:100%; text-aling:center">
			<img src="{{url('logogray.png')}}" alt="logo bo" width="80px" height="80px" style="float:left; border-radius: 5px">
			<img src="{{url('image/brands', $client_img)}}" alt="logo distribuyer" width="80px" height="80px" style="float:right; border-radius: 5px">
		</div><br><br>
	</header>
	<footer>
		<span>Copyright 2020. BO accesorios. Developed by Netizen one</span>
	</footer>
	<main>
		<table class="table table-striped table-bordered mt-4" style="width: 100%; margin-top: 50px !important">
			<thead>
			<tr>
				<th scope="col"><center>Ref</th>
				<th scope="col"><center>Nombre</th>
				<th scope="col"><center>Precio</th>
				<th scope="col"><center>Imágen</th>
			</tr>
			</thead>
			<tbody>
				@foreach($products as $key => $order)
				<tr>
					<td><br><center>{{$order->ref}}</center></td>
					<td><br><center>{{$order->product}}</center></td>
					<td><br><center>{{$order->price}}</center></td>
					<td>z
						<img style="width:150px; height:150px; margin-top: 1px" src="{{url('image/products', $order->img)}}">
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</main>
</body>
</html>