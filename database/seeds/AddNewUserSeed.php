<?php

use Illuminate\Database\Seeder;

class AddNewUserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'julianhrcorporation',
            'email' => 'Julian@hrcorporationzf.com',
            'password' => bcrypt('Julian2020*'),
            'user_type' => 1
        ]);
    }
}
