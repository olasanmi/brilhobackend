<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('cities')->insert([
            'name' => 'Medellin'
        ]);

        DB::table('cities')->insert([
            'name' => 'Bogota'
        ]);

        DB::table('cities')->insert([
            'name' => 'Cali'
        ]);

        DB::table('cities')->insert([
            'name' => 'Bucaramanga'
        ]);
    }
}
